/*

______  ___  _____       _____              _         _ 
|  ___|/   ||_   _|     /  ___|            (_)       | |
| |_  / /| |  | |  ___  \ `--.   ___  _ __  _   __ _ | |
|  _|/ /_| |  | | / _ \  `--. \ / _ \| '__|| | / _` || |
| |  \___  |  | || (_) |/\__/ /|  __/| |   | || (_| || |
\_|      |_/  \_/ \___/ \____/  \___||_|   |_| \__,_||_|
                                                        
    Copyright (C) F4ToSerial By Myoda, Inc - All Rights Reserved
    Unauthorized copying of this file, via any medium is strictly prohibited
    Proprietary and confidential
    Written by Alexandre Laurencin <laurencin.alexandre@gmail.com>, Nov 2017
    
    For more informations, please see : http://f4toserial.mini-cube.fr

*/

#include <ArduinoJson.h>
#include <AccelStepper.h>
#include <MultiStepper.h>
#include <StreamDebug.h>

#define BUFFERSIZE  2048
#define DEFAULT_ACCELERATION  1000
#define DEFAULT_MAXSPEED      1000
    
#define PHOTON      true
#define ALLOWDEBUG  false

#if PHOTON

StreamDebug* debug;
#define DEBUG(...)   debug->d(__VA_ARGS__)
#define ERROR(...)   debug->e(__VA_ARGS__)
#else
#define DEBUG(...)   Serial.println(__VA_ARGS__)
#endif

struct Motor {
    char* name;
    unsigned int acceleration;
    unsigned int maxSpeed;
    short steps;
    unsigned char pinsCount;
    unsigned char* pins;
    AccelStepper* stepper;
};

Motor* motors = NULL;
unsigned char motorsCount = 0;

void setup() {
    Serial.begin(2000000);
    #if PHOTON
    debug = new StreamDebug(&Serial);
    #endif

    #if ALLOWDEBUG 
    DEBUG("Initializing digital pins...");
    #endif
    for(int i = 0; i < 8; i++) {
        pinMode(i, OUTPUT);
    }
    #if ALLOWDEBUG 
    DEBUG("Digital pins initialized.");
    #endif
}

void loop() {
    static char buffer[BUFFERSIZE];
    if (readline(Serial.read(), buffer, BUFFERSIZE) > 0) {
        #if ALLOWDEBUG 
        DEBUG(buffer);
        #endif
        StaticJsonBuffer<BUFFERSIZE> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(buffer);
        if (!root.success()) {
            ERROR("Command MUST be a JSON object. Exiting.");
            return;
        }
        if(root.containsKey("setup_stepper")) {
            parse_setup_command(root["setup_stepper"]);
        }
        else if(root.containsKey("setstep")) {
            parse_setstep_command(root["setstep"]);
        }
        else {
            ERROR("Unknown command. Exiting.", motorsCount);
            return;
        }
    }
    for(int i = 0; i < motorsCount; i++) {
        motors[i].stepper->run();
    }
}

void parse_setup_command(JsonVariant json) {
    if(!json.is<JsonObject&>()) {
        ERROR("Command 'setup' MUST be a JSON object. Exiting.");
        return;
    }
    clear_motors();
    JsonObject& object = json;
    for (auto kv : object) {
        if(!kv.value.is<JsonObject&>()) {
            ERROR("Motor '%s': motor configuration MUST be a JSON object. Exiting.", kv.key);
            continue;
        }
        JsonObject& motorConfiguration = kv.value;
        // Name
        char* name = (char*) malloc((strlen(kv.key) + 1) * sizeof(char));
        memcpy(name, kv.key, (strlen(kv.key) + 1) * sizeof(char));
        // Steps
        if(!motorConfiguration.containsKey("steps")) {
            ERROR("Motor '%s': parameter 'steps' is mandatory. Exiting.", kv.key);
            continue;
        }
        if(!motorConfiguration["steps"].is<short>()) {
            ERROR("Motor '%s': parameter 'steps' MUST be a number. Exiting.", kv.key);
            continue;
        }
        short steps = motorConfiguration["steps"].as<short>();
        // Acceleration
        unsigned int acceleration = DEFAULT_ACCELERATION;
        if(motorConfiguration.containsKey("acceleration"))
            acceleration = motorConfiguration["acceleration"].as<unsigned int>();
        // Max speed
        unsigned int maxSpeed = DEFAULT_MAXSPEED;
        if(motorConfiguration.containsKey("maxSpeed"))
            maxSpeed = motorConfiguration["maxSpeed"].as<unsigned int>();
        // Pins count
        if(!motorConfiguration.containsKey("pinsCount")) {
            ERROR("Motor '%s': parameter 'pinsCount' is mandatory. Exiting.", kv.key);
            continue;
        }
        if(!motorConfiguration["pinsCount"].is<unsigned char>()) {
            ERROR("Motor '%s': parameter 'pinsCount' MUST be a number. Exiting.", kv.key);
            continue;
        }
        unsigned char pinsCount = motorConfiguration["pinsCount"].as<unsigned char>();
        // Pins configuration
        if(!motorConfiguration.containsKey("pins")) {
            ERROR("Motor '%s': parameter 'pins' is mandatory. Exiting.", kv.key);
            continue;
        }
        if(!motorConfiguration["pins"].is<JsonArray&>()) {
            ERROR("Motor '%s': parameter 'pins' MUST be an array. Exiting.", kv.key);
            continue;
        }
        JsonArray& pinsConfiguration = motorConfiguration["pins"].as<JsonArray&>();
        unsigned char* pins = (unsigned char*) malloc(pinsCount * sizeof(unsigned char));
        if(pinsConfiguration.size() != pinsCount) {
            ERROR("Motor '%s': set to %i pins, only %i passed. Exiting.", kv.key, pinsCount, pinsConfiguration.size());
            continue;
        }
        #if ALLOWDEBUG 
        DEBUG("Setting up motor '%s'...", kv.key);
        #endif

        for(int i = 0; i < pinsCount; i++) {
            pins[i] = pinsConfiguration[i].as<unsigned char>();
        }
        #if ALLOWDEBUG 
        DEBUG("Motor '%s' set up.", kv.key);
        #endif
        add_motor(name, steps, acceleration, maxSpeed, pinsCount, pins);
    }
    #if ALLOWDEBUG 
    DEBUG("%i motor(s) set up.", motorsCount);
    #endif
    reset_motors();
}

void parse_setstep_command(JsonVariant json) {
    if(!motorsCount) {
        ERROR("No motors found, please call 'setup'. Exiting.", motorsCount);
        return;
    }
    JsonObject& object = json;
    for (auto kv : object) {
        if(!kv.value.is<short>()) {
            ERROR("Parameter %s MUST be a number. exiting.", kv.key);
            continue;
        }
        bool stepsSet = false;
        short steps = kv.value.as<short>();
        #if ALLOWDEBUG 
        DEBUG("Setting motor %s steps to %i...", kv.key, steps);
        #endif
        for(int i = 0; i < motorsCount; i++) {
            Motor motor = motors[i];
            if(!strcmp(motor.name, kv.key)) {
                motor.stepper->moveTo(steps);
                stepsSet = true;
            }
        }
        if(!stepsSet) {
            #if ALLOWDEBUG 
            #endif DEBUG("Motor %s not found.", kv.key);
            continue;
        }
        #if ALLOWDEBUG 
        DEBUG("Motor %s steps set.", kv.key);
        #endif
    }
}

void reset_motors() {
    #if ALLOWDEBUG 
    DEBUG("Resetting %i motor(s)...", motorsCount);
    #endif
    MultiStepper* multiStepper = new MultiStepper();
    long int* downPositions = NULL;
    long int* upPositions = NULL;
    for(int i = 0; i < motorsCount; i++) {
        downPositions = (long int*) realloc(downPositions, (i+1) * sizeof(long int));
        downPositions[i] = -50;
        upPositions = (long int*) realloc(upPositions, (i+1) * sizeof(long int));
        upPositions[i] = motors[i].steps + 50;
        motors[i].stepper->setMaxSpeed(200);
        motors[i].stepper->setAcceleration(200);
        multiStepper->addStepper(*motors[i].stepper);
    }
    #if ALLOWDEBUG 
    DEBUG("Moving all motors to upper position...");
    #endif
    multiStepper->moveTo(upPositions);
    multiStepper->runSpeedToPosition();
    #if ALLOWDEBUG 
    DEBUG("All motors should be up.");
    #endif
    delay(300);
    #if ALLOWDEBUG 
    DEBUG("Moving all motors to lower position...");
    #endif
    multiStepper->moveTo(downPositions);
    multiStepper->runSpeedToPosition();
    #if ALLOWDEBUG
    DEBUG("All motors should be down.");
    #endif
    for(int i = 0; i < motorsCount; i++) {
        motors[i].stepper->setCurrentPosition(0);
        motors[i].stepper->setMaxSpeed(motors[i].maxSpeed);
        motors[i].stepper->setAcceleration(motors[i].acceleration);
    }
    free(downPositions);
    free(upPositions);
    free(multiStepper);
    #if ALLOWDEBUG 
    DEBUG("%i motor(s) reset.", motorsCount);
    #endif
}

void add_motor(char* name, unsigned short steps, unsigned int acceleration, unsigned int maxSpeed, unsigned char pinsCount, unsigned char* pins) {
    AccelStepper* stepper;
    if(pinsCount == 4) {
        stepper = new AccelStepper(pinsCount, pins[0], pins[1], pins[2], pins[3]);
    }
    else if(pinsCount == 3) {
        stepper = new AccelStepper(pinsCount, pins[0], pins[1], pins[2]);
    }
    else {
        stepper = new AccelStepper(pinsCount, pins[0], pins[1]);
    }
    motors = (Motor*) realloc(motors, (motorsCount + 1) * sizeof(Motor));
    motors[motorsCount].name = name;
    motors[motorsCount].steps = steps;
    motors[motorsCount].acceleration = acceleration;
    motors[motorsCount].maxSpeed = maxSpeed;
    motors[motorsCount].pinsCount = pinsCount;
    motors[motorsCount].pins = pins;
    motors[motorsCount].stepper = stepper;
    motorsCount++;
}

void clear_motors() {
    for(int i = 0; i < motorsCount; i++) {
        free(motors[i].name);
        free(motors[i].pins);
        free(motors[i].stepper);
    }
    free(motors);
    motors = NULL;
    motorsCount = 0;
}


int readline(int readch, char *buffer, int len) {
    static int pos = 0;
    int rpos;

    if (readch > 0) {
        switch (readch) {
            case '\n': // Ignore new-lines
                break;
            case '\r': // Return on CR
                rpos = pos;
                pos = 0;  // Reset position index ready for next time
                return rpos;
            default:
                if (pos < len-1) {
                    buffer[pos++] = readch;
                    buffer[pos] = 0;
                }
        }
    }
    // No end of line has been found, so return -1.
    return -1;
}